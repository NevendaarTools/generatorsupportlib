#ifndef GENERATORHANDLE_H
#define GENERATORHANDLE_H
#include <Windows.h>
#include <QDebug>
#include "CommonApiStructures.h"

struct GeneratorHandle
{
    GeneratorHandle()
        :name(nullptr), cleanup(nullptr)
    {}

    bool load(const wchar_t * filename)
    {
        libHandle = LoadLibrary(filename);
        if (!libHandle)
        {
            printf("Failed to load dll.");
            int error = GetLastError();
            printf("%d", error);
            return false;
        }
        init = (void (*)(void))GetProcAddress(libHandle, "init");
        if (!init)
        {
            printf("Failed to export \"init\" from DLL.");
            close();
            return false;
        }
        init();

        getName = (char* (*)(void))GetProcAddress(libHandle, "getName");
        if (!getName)
        {
            printf("Failed to export \"getName\" from DLL.");
            close();
            return false;
        }
        name = getName();

        getDescription = (char* (*)(void))GetProcAddress(libHandle, "getDescription");
        if (!getDescription)
        {
            printf("Failed to export \"getDescription\" from DLL.");
            close();
            return false;
        }
        desc = getDescription();

        getOptionsCount = (int (*)(void))GetProcAddress(libHandle, "getOptionsCount");

        if (!getOptionsCount)
        {
            printf("Failed to export \"getOptionsCount\" from DLL.");
            close();
            return false;
        }

        getOptionAt = (Option* (*)(int))GetProcAddress(libHandle, "getOptionAt");
        if (!getOptionAt)
        {
            printf("Failed to export \"getOptionAt\" from DLL.");
            close();
            return false;
        }
        setOptionAt = (int (*)(int, const char *))GetProcAddress(libHandle, "setOptionAt");
        if (!setOptionAt)
        {
            printf("Failed to export \"setOptionAt\" from DLL.");
            close();
            return false;
        }

        generateMap = (int (*)(void))GetProcAddress(libHandle, "generateMap");

        if (!generateMap)
        {
            printf("Failed to export \"generateMap\" from DLL.");
            close();
            return false;
        }

        return true;
    }
    bool close()
    {
        if (cleanup)
            cleanup();
        FreeLibrary(libHandle);
        return true;
    }

    void        (*init)             (void);
    char*       (*getName)          (void);
    char*       (*getDescription)   (void);
    int         (*getOptionsCount)  (void);
    Option*     (*getOptionAt)      (int);
    int         (*setOptionAt)      (int, const char*);
    int         (*generateMap)      (void);
    void        (*cleanup)          (void);

    char * name;
    char * desc;
    HMODULE libHandle;
};

#endif // GENERATORHANDLE_H
