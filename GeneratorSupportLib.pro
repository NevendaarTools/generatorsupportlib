QT += widgets
QT += quick
QT += quickwidgets
QT += gui-private

TEMPLATE = lib
DEFINES += GENERATORSUPPORTLIB_LIBRARY

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    GeneratorSupportLib.cpp \
    GeneratorWrapper.cpp \
    MapGenerationWidget.cpp \
    Utils/OptionsModel.cpp \
    Utils/ImageProvider.cpp \
    Utils/StringModel.cpp

HEADERS += \
    CommonApiStructures.h \
    GeneratorHandle.h \
    GeneratorSupportLib_global.h \
    GeneratorSupportLib.h \
    GeneratorWrapper.h \
    MapGenerationWidget.h \
    Utils/IConfigurable.h \
    Utils/IStringDataProvider.h \
    Utils/OptionsModel.h \
    Utils/IGSLWidget.h \
    Utils/ImageProvider.h \
    Utils/StringModel.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    QML/GeneratorOptionsDialog.qml

CONFIG(release, debug|release): DESTDIR = $$OUT_PWD/release
CONFIG(debug, debug|release): DESTDIR = $$OUT_PWD/debug

win32{
    QMAKE_EXTRA_TARGETS += qml_files images
    qml_files.target = qml_files
    qml_files.commands = $${QMAKE_COPY_DIR} $$shell_path($$PWD/QML) $$shell_path($$DESTDIR/QML)

    images.target = images
    images.commands = $${QMAKE_COPY_DIR} $$shell_path($$PWD/Images) $$shell_path($$DESTDIR/Images)

    POST_TARGETDEPS = qml_files images
#    QMAKE_POST_LINK += $$(QTDIR)/bin/windeployqt --printsupport --opengl --qmldir . --release $$shell_path($$DESTDIR/GeneratorSupportLib.dll)

    QMAKE_DEL_FILE = del /q /s
    QMAKE_CLEAN += $${DESTDIR}/QML/*.*
    QMAKE_CLEAN += $${DESTDIR}/Images/*.*
}
