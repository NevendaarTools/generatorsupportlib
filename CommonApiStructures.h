#ifndef COMMONAPISTRUCTURES_H
#define COMMONAPISTRUCTURES_H

#include <stdio.h>

struct Option
{
    enum Type
    {
        Bool = 0,
        Float,
        Int,
        String,
        Enum
    };

    char *  name;
    char *  desc;
    char *  value;
    char *  min;
    char *  max;
    char *  variants;
    int type;
};


typedef int32_t ErrorCode;

#endif // COMMONAPISTRUCTURES_H
