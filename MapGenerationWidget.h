#ifndef MAPGENERATIONWIDGET_H
#define MAPGENERATIONWIDGET_H

#include <QQuickWidget>
#include "Utils/IGSLWidget.h"
#include "Utils/OptionsModel.h"
#include "GeneratorHandle.h"
#include "Utils/StringModel.h"

class MapGenerationWidget : public QQuickWidget, public IGSLWidget
{
    Q_OBJECT
public:
    explicit MapGenerationWidget(QWidget *parent = nullptr);

    ~MapGenerationWidget();

    Q_INVOKABLE void selectGenerator(int index);
signals:


    // IGSLWidget interface
public:
    virtual void init(const QString & path, QQmlImageProviderBase *provider) override;
    virtual QQuickWidget *widget() override;
    Q_INVOKABLE void generate();
private:
    void initGenList();
private:
    StringModel * m_generatorsModel;
    OptionsModel * m_optionsModel;
    QVector <GeneratorHandle> m_generators;
};

#endif // MAPGENERATIONWIDGET_H
