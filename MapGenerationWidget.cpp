#include "MapGenerationWidget.h"
#include <QDir>
#include <QFileInfo>
#include <QQmlContext>
#include <qDebug>
#include "GeneratorWrapper.h"

MapGenerationWidget::MapGenerationWidget(QWidget *parent)
    : QQuickWidget(parent)
{
    m_optionsModel = new OptionsModel(this);
    m_generatorsModel = new StringModel(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint | Qt::FramelessWindowHint);
}

MapGenerationWidget::~MapGenerationWidget()
{
    for (int i = 0; i < m_generators.count(); ++i)
    {
        m_generators[i].close();
    }
}

void MapGenerationWidget::selectGenerator(int index)
{
    if (index < 0 || index >= m_generators.count())
        return;
    m_optionsModel->setConfigurable(QSharedPointer<IConfigurable>(new GeneratorWrapper(&m_generators[index])));
}


void MapGenerationWidget::init(const QString & path, QQmlImageProviderBase * provider)
{
    engine()->addImageProvider(QLatin1String("provider"), provider);
    rootContext()->setContextProperty("ImagePath", "image://provider/");

    rootContext()->setContextProperty("generatorsModel", QVariant::fromValue((QObject*)m_generatorsModel));
    rootContext()->setContextProperty("optionsModel", QVariant::fromValue((QObject*)m_optionsModel));
    rootContext()->setContextProperty("genWindow", QVariant::fromValue((QObject*)this));
    setSource(QUrl::fromLocalFile(path + QStringLiteral("/GeneratorOptionsDialog.qml")));
    setResizeMode(QQuickWidget::SizeRootObjectToView);
    initGenList();
}

QQuickWidget *MapGenerationWidget::widget()
{
    return this;
}

void MapGenerationWidget::generate()
{
    QSharedPointer<IConfigurable> gen = m_optionsModel->configurable();
    GeneratorWrapper * wrapper = static_cast<GeneratorWrapper*>(gen.data());
    wrapper->generate();
    this->close();
}

void MapGenerationWidget::initGenList()
{
    QDir dir("../Generators/", "*.dll");
    QFileInfoList genNames = dir.entryInfoList();
    SimpleStringProvider * provider = new SimpleStringProvider();
    foreach (const QFileInfo& name, genNames)
    {
        qDebug()<<name;
        GeneratorHandle gen;
        if  (gen.load(name.absoluteFilePath().toStdWString().c_str()))
        {
            m_generators.append(gen);
            provider->addRow(m_generators.last().name, m_generators.last().desc);
        }
    }


    QSharedPointer<IStringDataProvider> prov(provider);
    m_generatorsModel->setProvider(prov);
    if (m_generators.count() > 0)
        selectGenerator(0);
}
