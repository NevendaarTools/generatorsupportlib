// DllLoader.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <conio.h>
#include <ctype.h>

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg) {
	case WM_DESTROY:// ���� ����� �� �������, �� ��� ���� ������ ������� ������� ���� ����� ���������������
		PostQuitMessage(0);// ���������� ���������� ��������� WM_QUIT. ��������� ��� ������, ������� ��������� � wParam ��������� WM_QUIT
		break;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);//������������ ��� ��������� ��������� ������������ "�� ���������"
}

//int _tmain(int argc, _TCHAR* argv[])
int WINAPI WinMain(HINSTANCE hInst,	//����� �� ��� ����������
	HINSTANCE hPrev,		//�������� ��� ������������� � Win16, ������ = 0
	LPSTR szCmdLine,		//��������� ������ ����� ����������
	int nShowCmd)			//���������, �����������, ��� ���� ��������� ����������
{

	WNDCLASSEX wcx = { 0 };//�������� ����� ��� ���� ���������, ����� ������ �� ������, �.�. ����������� ��� ���� �� ���// � �� ������� ��� WNDCLASSEX ����� �� �����, �� MSDN ��������
	wcx.cbSize = sizeof(WNDCLASSEX); //�� ������� ��������� Windows ���������, ����� ������ API ���� ������������
	wcx.style = CS_HREDRAW | CS_VREDRAW;// ������� ���� ���������������� ��� ��������� �������� ����
	wcx.lpfnWndProc = WndProc;// ��������� ������� ��������� ���������
	wcx.hInstance = hInst;	// ����� �� ���� ����������
	wcx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); // GetStockObject ���������� ����� �� ����� ��������, ��� ���� ����
	wcx.lpszClassName = TEXT("[lexpenz.com] Win32.");// ��� ������� ������. ������ ���� ����������, �����, ���� ����� � ����� ������ ��� ���������������, �� � ����������� ����� ��������

	if (!RegisterClassEx(&wcx))
		return 1;// ������������ ( �� ���������� - ������ �� ��������� ) � ����� ������ (1)

	// 2� ����
	// ��������� ����
	HWND hWnd = CreateWindowEx(0,
		TEXT("[lexpenz.com] Win32."),//��� ������
		TEXT("[lexpenz.com] Win32. ������ ���������� Win32."),//��������� ����
		WS_OVERLAPPEDWINDOW, //��� ������ (�������� ����������� ���������� ����, ������ � ������� ������ ���� � �.�.)
		CW_USEDEFAULT, 0,//����� ��������� ���� (���������� � � y). ����� ������� ����� ��� ����������, ������� ������ �������� ������������
		CW_USEDEFAULT, 0,//������ ���� (������������ ���������� ����� ���������)
		0, //������ �� ������������ ����
		0,//����� ����
		hInst,
		0);//���, ������������ � ���������� WM_CREATE

	if (!hWnd)    //��������� ���������� �������� ����
		return 2;

	// ������ ���������� ������ ( nShowCmd - ��� ��� ��������? ����������������, ������� ��� ... )
	ShowWindow(hWnd, nShowCmd);
	// ������� ���� ����������
	UpdateWindow(hWnd);

	//HMODULE library = LoadLibrary(L"C:\\Users\\Dimon\\Documents\\build-TestDialogLib-Desktop_Qt_5_13_0_MSVC2017_64bit3-Debug\\debug\\TestDialogLib.dll");
	//HMODULE library = LoadLibrary(L"C:\\Users\\Dimon\\Documents\\TestDialogLib\\build\\32bit\\release\\TestDialogLib.dll");
	//HMODULE library = LoadLibrary(L"TestSettings\\TestDialogLib.dll");
	HMODULE library = LoadLibrary(L"GeneratorSupportLib.dll");
	
	if (!library)
	{
		printf("Failed to load dll.");
		int error = GetLastError();
		printf("%d", error);
		return 0;
	}
	printf("Dll loaded.\n");

	HWND(*dllInstance) (void);
	dllInstance = (HWND (*)(void))GetProcAddress(library, "test");
	
	if (!dllInstance)
	{
		printf("Not export \"test\" from DLL.");
		return 0;
	}
	
	HWND handle = dllInstance();
	//HWND currentHandle = GetActiveWindow();
	HWND currentHandle = FindWindowEx(NULL, NULL, L"[lexpenz.com] Win32.", NULL);
	HWND oldHandle = SetParent(handle, currentHandle);
	ShowWindowAsync(handle, SW_SHOWMAXIMIZED);

	//getch();
	// 3� ����
// ������ �������� ����� ��������� ���������
	MSG msg = { 0 };// ������� ��������� ���������, ������� ����� ������������
	while (GetMessage(&msg,
		0,//������� �������� ��������� �� ���� ����
		0, 0))//�������� �������� ���������� ��������� (������ �������� ���)
	{	// ���� ���������
		TranslateMessage(&msg);	// ����������� ����������� ������� � ASCII-��� � �������� ��������� WM_CHAR (��� �� �����.����������, ���� ���� �������� � �������, �������� � ����������)
		DispatchMessage(&msg);	// �������� ��������� ��� ��������� � "������� ������� ��������� ���������"
	}
	FreeLibrary(library);
	return 0;
}

