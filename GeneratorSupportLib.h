#ifndef GENERATORSUPPORTLIB_H
#define GENERATORSUPPORTLIB_H

#include "GeneratorSupportLib_global.h"
#include <Windows.h>
#include <QApplication>
#include "Utils/ImageProvider.h"

class GeneratorSupportLib
{
public:
    static GeneratorSupportLib * instance();
#ifdef GENERATORSUPPORTLIB_LIBRARY
    HWND mapGenerationWindow();
#endif
    QWidget * genWidget();
    bool init();
    bool uninit();
    QApplication *app() const;

private:
    GeneratorSupportLib();

    QApplication * m_app;
    bool m_initialised;
    ImageProvider m_provider;
    QString m_path;

    //
    char * m_argv[1];
    int m_argc;
};

#endif // GENERATORSUPPORTLIB_H
