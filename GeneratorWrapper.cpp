#include "GeneratorWrapper.h"
#include <QStringList>

GeneratorWrapper::GeneratorWrapper(GeneratorHandle *handle)
    : m_handle(handle)
{

}

int GeneratorWrapper::optionsCount() const
{
    return m_handle->getOptionsCount();
}

OptionQt GeneratorWrapper::getOptionAt(int index)
{
    OptionQt result;
    Option * opt = m_handle->getOptionAt(index);
    result.desc = QString(opt->desc);
    result.name = QString(opt->name);
    result.min = QString(opt->min);
    result.max = QString(opt->max);
    result.value = QString(opt->value);
    result.type = opt->type;
    QString variantsString = QString(opt->variants);
    if (variantsString.count() > 0)
        result.variants = variantsString.split(";");
    return result;
}

bool GeneratorWrapper::setOptionValue(int index, const QString &value)
{
    m_handle->setOptionAt(index, value.toStdString().c_str());
    return true;
}

void GeneratorWrapper::generate()
{
    qDebug()<<"Generate";
    m_handle->generateMap();
}
