#ifndef ISTRINGDATAPROVIDER_H
#define ISTRINGDATAPROVIDER_H
#include <QString>
#include <QVector>

class IStringDataProvider
{
public:
    ~IStringDataProvider(){}
    virtual int dataCount() const = 0;
    virtual QString getName(int index) const = 0;
    virtual QString getDesc(int index) const = 0;
};


class SimpleStringProvider: public IStringDataProvider
{
public:
    SimpleStringProvider(){}

    struct DataRow
    {
        QString name;
        QString desc;
    };
    void addRow(const QString & name, const QString& desc)
    {
        m_data.append(DataRow{name, desc});
    }
    void clear()
    {
        m_data.clear();
    }
    // IStringDataProvider interface
public:
    virtual int dataCount() const override
    {
        return m_data.count();
    }
    virtual QString getName(int index) const override
    {
        return m_data[index].desc;
    }
    virtual QString getDesc(int index) const override
    {
        return m_data[index].desc;
    }
private:
    QVector<DataRow> m_data;
};







#endif // ISTRINGDATAPROVIDER_H
