#ifndef IMAGEPROVIDER_H
#define IMAGEPROVIDER_H

#include <qquickimageprovider.h>
#include <QMap>
#include <qstring.h>
#include <QImage>
#include <QPixmap>
#include <QDir>
#include <QDebug>

class ImageProvider : public QQuickImageProvider
{
public:
    ImageProvider();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);
    void registerImage(const QString& name, const QImage& image);
private:
    QMap <QString, QImage> m_imageMap;
};

#endif // IMAGEPROVIDER_H
