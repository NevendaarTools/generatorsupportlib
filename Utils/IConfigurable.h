#ifndef ICONFIGURABLE_H
#define ICONFIGURABLE_H
#include <QString>
#include <QStringList>

struct OptionQt
{
    enum Type
    {
        Bool = 0,
        Float,
        Int,
        String,
        Enum
    };

    QString name;
    QString  desc;
    QString  value;
    QString  min;
    QString  max;
    QStringList variants;
    int type;
};

class IConfigurable
{
public:
    virtual ~IConfigurable(){}
    virtual int optionsCount() const = 0;
    virtual OptionQt getOptionAt(int index) = 0;
    virtual bool setOptionValue(int index, const QString& value) = 0;
};

#endif // ICONFIGURABLE_H
