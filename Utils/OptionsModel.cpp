#include "OptionsModel.h"

OptionsModel::OptionsModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_TYPE] = "OptionType";
    m_roles[ROLE_VALUE] = "Id";
    m_roles[ROLE_DESC] = "Desc";
    m_roles[ROLE_VALUE] = "Value";
}

QSharedPointer<IConfigurable> OptionsModel::configurable() const
{
    return m_configurable;
}

void OptionsModel::setConfigurable(const QSharedPointer<IConfigurable> &configurable)
{
    beginResetModel();
    m_configurable = configurable;
    endResetModel();
}

int OptionsModel::rowCount(const QModelIndex &parent) const
{
    if (m_configurable.isNull() || parent.isValid())
        return 0;
    return m_configurable->optionsCount();
}

QVariant OptionsModel::data(const QModelIndex &index, int role) const
{
    const OptionQt info = m_configurable->getOptionAt(index.row());
    if (role == ROLE_NAME)
    {
        return info.name;
    }
    if (role == ROLE_TYPE)
    {
        return info.type;
    }
    if (role == ROLE_VALUE)
    {
        return info.value;
    }
    if (role == ROLE_DESC)
    {
        return info.desc;
    }
    return QVariant();
}

QHash<int, QByteArray> OptionsModel::roleNames() const
{
    return m_roles;
}

void OptionsModel::resetModel()
{
    beginResetModel();
    endResetModel();
}

void OptionsModel::setValue(int index, const QString &value, bool withReload)
{
    if (withReload)
    {
        beginResetModel();
        m_configurable->setOptionValue(index, value);
        endResetModel();
    }
    else
    {
        m_configurable->setOptionValue(index, value);
        //emit dataChanged(this->index(index, 0), this->index(index, 0));
    }
}

int OptionsModel::variantsCount(int index) const
{
    if (index < 0)
        return 0;
    return m_configurable->getOptionAt(index).variants.count();
}

void OptionsModel::nextVariant(int index)
{
    if (index < 0)
        return;
    OptionQt opt = m_configurable->getOptionAt(index);
    int varIndex = opt.variants.indexOf(opt.value);
    if (varIndex == -1 || varIndex + 1 >= opt.variants.count())
        return;
    setValue(index, opt.variants[varIndex + 1]);
}

void OptionsModel::prevVariant(int index)
{
    if (index < 0)
        return;
    OptionQt opt = m_configurable->getOptionAt(index);
    int varIndex = opt.variants.indexOf(opt.value);
    if (varIndex <= 0)
        return;
    setValue(index, opt.variants[varIndex - 1]);
}

int OptionsModel::currentVariantIndex(int index) const
{
    if (index < 0)
        return 0;
    OptionQt opt = m_configurable->getOptionAt(index);
    return opt.variants.indexOf(opt.value);
}
