#ifndef IGSLWIDGET_H
#define IGSLWIDGET_H
#include <QQuickWidget>
#include <QQmlApplicationEngine>
#include <QQmlImageProviderBase>

class IGSLWidget
{
public:
    virtual ~IGSLWidget(){}
    virtual void init(const QString & path, QQmlImageProviderBase *provider) = 0;
    virtual QQuickWidget * widget () = 0;
};

#endif // IGSLWIDGET_H
