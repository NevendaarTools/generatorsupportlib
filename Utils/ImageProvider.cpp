#include "ImageProvider.h"

ImageProvider::ImageProvider() : QQuickImageProvider(QQuickImageProvider::Image)
{

}

QImage ImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    Q_UNUSED(size);
    QImage im = m_imageMap.value(id); //TODO: uncomment
//    if (im.isNull()) {
//        im = m_imageMap.value("cancel");
//    }
    if (requestedSize.width() > 0 && requestedSize.height() > 0) {
        return im.scaled(requestedSize);
    }
    return im;
}

void ImageProvider::registerImage(const QString &name, const QImage &image)
{
    if (image.isNull())
    {
        qDebug()<<"Failed to registerImage - " << name;
    }
    else
        m_imageMap.insert(name, image);
}
