#ifndef OPTIONSMODEL_H
#define OPTIONSMODEL_H

#include <QObject>
#include "IConfigurable.h"
#include <QAbstractListModel>
#include <QSharedPointer>

class OptionsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit OptionsModel(QObject *parent = nullptr);

    enum Roles
    {
        ROLE_NAME = Qt::UserRole + 1,
        ROLE_TYPE,
        ROLE_VALUE,
        ROLE_DESC
    };

    QSharedPointer<IConfigurable> configurable() const;
    void setConfigurable(const QSharedPointer<IConfigurable> &configurable);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Q_INVOKABLE void resetModel();
    Q_INVOKABLE void setValue(int index, const QString& value, bool withReload = true);
    Q_INVOKABLE int variantsCount(int index) const;
    Q_INVOKABLE void nextVariant(int index);
    Q_INVOKABLE void prevVariant(int index);
    Q_INVOKABLE int currentVariantIndex(int index) const;

signals:

private:
    QHash<int, QByteArray>      m_roles;
    QSharedPointer<IConfigurable> m_configurable;
};

#endif // OPTIONSMODEL_H
