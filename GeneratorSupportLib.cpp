#include "GeneratorSupportLib.h"
#include <QWindow>
#include <QWidget>
#include <QGuiApplication>
#include <QDebug>
#include <QGuiApplication>
#include <QQuickWidget>
#include "MapGenerationWidget.h"
#include <QDir>

#ifdef GENERATORSUPPORTLIB_LIBRARY
#include <qpa/qplatformnativeinterface.h>


QWindow * windowForWidget( const QWidget * widget )
{
  QWindow * window = widget->windowHandle();
  if( window )
    return window;

  const QWidget * nativeParent = widget->nativeParentWidget();
  if( nativeParent )
    return nativeParent->windowHandle();

  return nullptr;
}

HWND getHWNDForWidget( const QWidget * widget )
{
  QWindow * window = ::windowForWidget( widget );
  if( window && window->handle() )
  {
    QPlatformNativeInterface * iface = QGuiApplication::platformNativeInterface();
    return static_cast<HWND>(iface->nativeResourceForWindow( QByteArrayLiteral( "handle" ), window ));
  }

  return nullptr;
}

HWND GeneratorSupportLib::mapGenerationWindow()
{
    return getHWNDForWidget(genWidget());
}

#endif

GeneratorSupportLib *GeneratorSupportLib::instance()
{
    static GeneratorSupportLib instance;
    return &instance;
}

QWidget *GeneratorSupportLib::genWidget()
{
    init();

    MapGenerationWidget * widget = new MapGenerationWidget (NULL);
    widget->init(m_path + "/QML", &m_provider);
    widget->setGeometry(450,450,600,400);
    widget->show();
    return widget;
}

bool GeneratorSupportLib::init()
{
    if (m_initialised)
        return true;

    m_argv[0] = new char [3];

    strncpy(m_argv[0], "./\0", 3);;//QDir::currentPath().toStdString().c_str();
    m_app = new QApplication (m_argc, m_argv);
    m_path = QCoreApplication::applicationDirPath();// + "/GeneratorSupportLib";

    m_provider.registerImage("back", QImage(m_path + "/Images/back.png"));
    m_provider.registerImage("true", QImage(m_path + "/Images/true.png"));
    m_provider.registerImage("false", QImage(m_path + "/Images/false.png"));
    m_provider.registerImage("left", QImage(m_path + "/Images/left.png"));
    m_provider.registerImage("right", QImage(m_path + "/Images/right.png"));

    m_initialised = true;
    return true;
}

bool GeneratorSupportLib::uninit()
{
    if (!m_initialised)
        return true;
    m_app->deleteLater();
    return true;
}

GeneratorSupportLib::GeneratorSupportLib()
    : m_app(nullptr), m_initialised(false)
{

}

QApplication *GeneratorSupportLib::app() const
{
    return m_app;
}


#ifdef GENERATORSUPPORTLIB_LIBRARY
extern "C" GENERATORSUPPORTLIB_EXPORT  HWND  test()
{
    return GeneratorSupportLib::instance()->mapGenerationWindow();
}

extern "C" GENERATORSUPPORTLIB_EXPORT  bool  init()
{
    return GeneratorSupportLib::instance()->init();
}

extern "C" GENERATORSUPPORTLIB_EXPORT  bool  uninit()
{
    return GeneratorSupportLib::instance()->uninit();
}

extern "C" GENERATORSUPPORTLIB_EXPORT  HWND showMapGenerationDialog(const int&, const int&)
{
    return GeneratorSupportLib::instance()->mapGenerationWindow();
}
#endif
