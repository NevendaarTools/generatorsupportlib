import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    id: wnd
    BorderImage{
        border { left: 20; top: 20; right: 20; bottom: 20 }
        //horizontalTileMode: BorderImage.Stretch
        //verticalTileMode: BorderImage.Stretch
        source: ImagePath + "back"
        anchors.fill: parent
        visible: true
        //opacity: 0.8
    }
    signal itemClicked(string _id)
    function clearSelected() {
        inventory1Grid.currentIndex = -1;
    }
    Text {
        id: title
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 20
        anchors.topMargin: 30
        antialiasing: true
        height: 30
        text: "Настройки генерации: "
        //style: Text.Sunken
        horizontalAlignment: Text.AlignHCenter
        font: Qt.font({
                          //family: fontfamily,
                          bold: true,
                          //italic: true,
                          pointSize: 12
                      });
        color: "#404040"
    }

    GridView{
        id:inventory1Grid

        flow: GridView.FlowTopToBottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: title.bottom
        anchors.bottom: parent.bottom
        anchors.margins: 10
        anchors.bottomMargin: 20
        anchors.topMargin: 30
        //ScrollBar.vertical: ScrollBar {policy: ScrollBar.AlwaysOn }

        cellWidth: 240; cellHeight: 24
        model: optionsModel
        clip:true
        Component {
            id: itemOnUnitDelegate
            Rectangle {
                id: wrapper
                width: 240
                height: 24
//                border.color: GridView.isCurrentItem ? "#006600" : "#d1c7a2"
                //border.color: "#77aa77"
                border.width: 0
                color: "transparent"
                BorderImage{
                    border { left: 10; top: 10; right: 10; bottom: 10 }
                    horizontalTileMode: BorderImage.Stretch
                    verticalTileMode: BorderImage.Stretch
                    source: ImagePath + "back"
                    anchors.fill: parent
                    visible: true
                    opacity: 0.8
                }
                Text {
                    id: nameText
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.margins: 5
                    width: 100
                    text: Name
                    wrapMode: Text.Wrap
                    antialiasing: true
                    //style: Text.Sunken

                    horizontalAlignment: Text.AlignHCenter
                    font: Qt.font({
                                      //family: fontfamily,
                                      bold: true,
                                      //italic: true,
                                      pointSize: 10
                                  });
                    color: "#404040"
                    HoverHandler {
                        onHoveredChanged: {
                            desc.visible = hovered
                            if (hovered)
                            {
                                desc.x = Math.min(parent.width / 2 + inventory1Grid.x + wrapper.x, inventory1Grid.width - desc.width)
                                desc.y = parent.height / 2 + inventory1Grid.y + wrapper.y - inventory1Grid.contentY
                                //desc.parent = parent
                                desc.name = Name
                                desc.desc = Desc
                                //desc.z = -100
                            }
                        }
                    }
                }
                Loader {
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.left: nameText.right
                        sourceComponent: {
                            switch(OptionType) {
                                case 0: return boolDelegate
                                case 2: return intDelegate
                                case 3: return stringDelegate
                                case 4: return variantsDelegate
                            }
                        }
                    }
                Component {
                    id: stringDelegate
                    Item{
                        anchors.fill: parent
                        Rectangle {
                            anchors.fill: parent
                            border.width: 1
                            border.color: "#000000"
                            color: "#00000000"
                            clip: false
                            anchors.margins: 4
                            TextEdit {
                                anchors.fill: parent
                                anchors.leftMargin: 4
                                anchors.rightMargin: 4
                                text: Value
                                horizontalAlignment: Text.AlignCenter
                                verticalAlignment: Text.AlignVCenter
                                //color: "#bbddbb"
                                font: Qt.font({
                                                  //family: fontfamily,
                                                  //bold: true,
                                                  //italic: true,
                                                  pointSize: 10})
                                focus: true
                                clip: true

                                onTextChanged: {
                                    optionsModel.setValue(index, text, false)
                                }

                            }
                        }

                    }
                }

                Component {
                    id: boolDelegate
                    Item
                    {
                        anchors.fill: parent
                        Image {
                            anchors.centerIn: parent

                            width: 60
                            height: 18

                            source: (Value == "True")?ImagePath + "true":ImagePath + "false"
                            MouseArea
                            {
                                anchors.fill: parent
                                onClicked: {
                                    optionsModel.setValue(index, (Value == "True")?"False":"True")
                                }
                            }
                        }
                    }
                }
                Component {
                    id: intDelegate
                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 4
                        width: 120
                        Text {
                            anchors.fill: parent
                            text: "int - " + Name
                        }
                    }
                }
                Component {
                    id: variantsDelegate
                    Item {
                        anchors.fill: parent
                        anchors.margins: 4
                        //source: ImagePath + "back"
                        Image {
                            id: leftButton
                            anchors.top: parent.top
                            anchors.left: parent.left
                            anchors.bottom: parent.bottom
                            anchors.margins: 0
                            width: 24
                            enabled: optionsModel.currentVariantIndex(index) > 0
                            opacity: enabled? 1: 0.4
                            source: ImagePath + "left"
                            MouseArea
                            {
                                anchors.fill: parent
                                onClicked: {
                                    optionsModel.prevVariant(index)
                                }
                            }
                        }
                        Image {
                            id: rightButton
                            anchors.top: parent.top
                            anchors.right: parent.right
                            anchors.bottom: parent.bottom
                            anchors.margins: 0
                            width: 24
                            enabled: optionsModel.currentVariantIndex(index) < optionsModel.variantsCount(index) - 1
                            opacity: enabled? 1: 0.4
                            source: ImagePath + "right"
                            MouseArea
                            {
                                anchors.fill: parent
                                onClicked: {
                                    optionsModel.nextVariant(index)
                                }
                            }
                        }
                        Text {
                            anchors.top: parent.top
                            anchors.left: leftButton.right
                            anchors.right: rightButton.left
                            anchors.bottom: parent.bottom
                            anchors.margins: 4
                            text: Value
                            wrapMode: Text.NoWrap
                            horizontalAlignment: Text.AlignHCenter
                            //color: "#bbddbb"
                            font: Qt.font({
                                              //family: fontfamily,
                                              //bold: true,
                                              //italic: true,
                                              pointSize: 8})
                        }
                    }
                }
            }
        }
        delegate: itemOnUnitDelegate
    }
    Item {
        id: desc
        property alias name: nameText.text
        property alias desc: descText.text

        visible: false
        width: 220
        height: 140
        z: 120
        BorderImage{
            border { left: 10; top: 10; right: 10; bottom: 10 }
            horizontalTileMode: BorderImage.Stretch
            verticalTileMode: BorderImage.Stretch
            source: ImagePath + "back"
            anchors.fill: parent
            visible: true
            opacity: 1
        }
        Text {
            id: nameText
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 5
            anchors.topMargin: 15
            height: 24
            text: ""
            wrapMode: Text.Wrap
            antialiasing: true
            //style: Text.Sunken

            horizontalAlignment: Text.AlignHCenter
            font: Qt.font({
                              //family: fontfamily,
                              bold: true,
                              //italic: true,
                              pointSize: 10
                          });
            color: "#404040"
        }
        Text {
            id: descText
            anchors.top: nameText.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 10
            anchors.topMargin: 15
            text: ""
            //style: Text.Sunken
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            font: Qt.font({
                              //family: fontfamily,
                              //bold: true,
                              //italic: true,
                              pointSize: 8
                          });
            color: "#404040"
        }
    }
    Image {
        id: closeButton
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin:  25
        height: 30
        width: 120
        source: ImagePath + "back"
        MouseArea {
            id: mouseArea
            anchors.fill: closeButton
            hoverEnabled: true

            onClicked: {
                genWindow.generate();
            }
        }
        Text {
            anchors.fill: parent
            anchors.margins: 6
            text: "Generate"
            //style: Text.Sunken
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            font: Qt.font({
                              //family: fontfamily,
                              bold: true,
                              //italic: true,
                              pointSize: 14
                          });
            color: "#404040"
        }

    }
}
