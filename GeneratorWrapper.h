#ifndef GENERATORWRAPPER_H
#define GENERATORWRAPPER_H
#include "Utils/IConfigurable.h"
#include "GeneratorHandle.h"

class GeneratorWrapper : public IConfigurable
{
public:
    GeneratorWrapper(GeneratorHandle * handle);

    // IConfigurable interface
public:
    virtual int optionsCount() const override;
    virtual OptionQt getOptionAt(int index) override;
    virtual bool setOptionValue(int index, const QString &value) override;
    void generate();
private:
    GeneratorHandle * m_handle;
};

#endif // GENERATORWRAPPER_H
